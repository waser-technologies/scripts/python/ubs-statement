# UBS Statement

This script converts an UBS statement export CSV file into a bank statement import CSV file for Accounting app of Web.mvc.

## Usage

This script doesn't have any dependecy. You can still run this script within a virtual environment by using the following command.

You'll need direnv for that.

```
cp .env.exemple .env
direnv allow .
```

You can now use the script.

```
python ubs_export_to_bank_statement_csv.py -i <INPUT_FILE> -o <OUTPUT_FILE>
```