import sys, getopt

def amount(amount_str):
	if amount_str != '':
		amount_float = float(amount_str.replace("'", ""))
		return amount_float
	return amount_str

def load_csv(filename):
	csv = {}
	f = open(filename, 'r')
	l = f.read()
	f.close()
	transactions = l.split('\n')

	for t in transactions[1:-4]:
		line = {}
		ts = t.split(';')

		transaction_id = ts[15]

		if not csv.get(transaction_id, None):
			csv[transaction_id] = {}

		if not csv[transaction_id].get('date', None):
			csv[transaction_id]['date'] = ts[11]
		if not csv[transaction_id].get('amount_out', None):
			csv[transaction_id]['amount_out'] = ts[19].replace("'", "")
		if not csv[transaction_id].get('amount_in', None):
			csv[transaction_id]['amount_in'] = ts[18].replace("'", "")
		if not csv[transaction_id].get('description', None):
			csv[transaction_id]['description'] = ts[12]

	# remove zero amount transacitons
	to_del = [] #list of transacitons id to delete from csv
	for k, v in csv.items():
		is_null = False
		if v.get('amount_out', None) and amount(v.get('amount_out')) == 0.:
			is_null = True
		if v.get('amount_in', None) and amount(v.get('amount_in')) == 0.:
			is_null = True
		if is_null:
			to_del.append(k)
	
	for d in to_del:
		del csv[d]
		
	return csv

def convert_to_statement(csv):
	return csv

def dump_statement(csv, filename):
	csv_str = "date,amount_in,amount_out,description\n"

	for _, l in csv.items():
		l_str = ",".join([s for _, s in l.items()])
		csv_str += l_str + "\n"
	
	f = open(filename, 'w')
	f.write(csv_str)
	f.close()

def main(argv):
	inputfile = ''
	outputfile = ''
	try:
		opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
	except getopt.GetoptError:
		print('ubs_export_to_bank_statement_csv.py -i <inputfile> -o <outputfile>')
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print('ubs_export_to_bank_statement_csv.py -i <inputfile> -o <outputfile>')
			sys.exit()
		elif opt in ("-i", "--ifile"):
			inputfile = arg
		elif opt in ("-o", "--ofile"):
			outputfile = arg
    
	ubs_csv = load_csv(inputfile)
	csv = convert_to_statement(ubs_csv)
	dump_statement(csv, outputfile)

	sys.exit()

if __name__ == "__main__":
   main(sys.argv[1:])